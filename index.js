"use strict";
const soap = require('soap');
const axios = require("axios");
const xml2js = require('xml2js');

class Utils {

    constructor() {
        this.token = false;
        this.tokenType = "BEARER"
    }

    async client(wsdl) {
        this.wsdl = wsdl;
        let option = {
            wsdl_options: {
                timeout: 10000
            }
        }
        if(this.token){
            option['wsdl_headers'] = {
                Authorization:  "Authorization: Bearer " + this.token
            }
        }
        return await soap.createClientAsync(this.wsdl, option).then((client) => {
            return client;
        }).catch(function (reason) {
            console.log("Error al obtener soap");
            console.log(reason);
            return false;
        });
    }

    setToken(token) {
        this.token = token;
    }

    setTokenType(type) {
        this.tokenType = type;
    }

    setWsdl(wsdl) {
        this.wsdl = wsdl;
    }

    cleanScheme(obj) {
        let objClean = [];
        if(typeof obj["soapenv:Envelope"] !== "undefined"){
            objClean = obj["soapenv:Envelope"];
            if(typeof obj["soapenv:Envelope"]['soapenv:Body'] !== "undefined"){
                objClean = obj["soapenv:Envelope"]['soapenv:Body'];
            }
        } else if (obj["soap:Envelope"] !== "undefined"){
            objClean = obj["soap:Envelope"];
            if(typeof obj["soap:Envelope"]['soap:Body'] !== "undefined"){
                objClean = obj["soap:Envelope"]['soap:Body'];
            }
        }
        if (objClean[0] !== "undefined"){
            return objClean[0];
        } else {
            return objClean;
        }
    }

    async post(url, xml, soapAction) {
        return new Promise(async (resolve, reject) => {
            const header = {
                headers: {
                    "content-type": "text/xml; charset=utf-8",
                    "Authorization": "Bearer " + this.token,
                    "SOAPAction": soapAction
                }
            };

            if (this.token !== false) {
                if (this.tokenType === 'BEARER') {
                    header.headers['Authorization'] = "Bearer " + this.token;
                } else {
                    header.headers['Authorization'] = this.token;
                }
            }

            try {

                let responseJSON = {
                    "success": false,
                    "message": "Sin respuesta",
                    "data": {}
                };

                const response = await axios.post(url, xml, header).then(function (r) {
                    return {
                        "success": true,
                        "data": r.data
                    }
                }).catch(error => {
                    if (error.response) {
                        return {
                            "success": false,
                            "message": "response",
                            "data": {
                                "code": error.response.status,
                                "headers": error.response.headers,
                                "data": error.response.data,
                            }
                        };
                    } else if (error.request) {
                        return {
                            "success": false,
                            "message": "request",
                            "data": {
                                "code": "NONE",
                                "request": error.request
                            }
                        };
                    } else {
                        return {
                            "success": false,
                            "message": "message",
                            "data": {
                                "code": "NONE",
                                "request": error.message
                            }
                        };
                    }
                });
                if (response.success) {
                    responseJSON.message = "Resultado SOAP"
                    responseJSON.success = true;
                    if(response.data && response.data.length > 0) {
                        xml2js.parseString(response.data, (err, result) => {
                            if (err) {
                                throw err;
                            }
                            let obj = this.cleanScheme(result);
                            if (typeof obj['soap:Fault'] !== "undefined") {
                                responseJSON.data = this.cleanXMl(obj['soap:Fault']);
                                responseJSON.success = false;
                            } else if (typeof obj['soapenv:Fault'] !== "undefined") {
                                responseJSON.data = this.cleanXMl(obj['soapenv:Fault']);
                                responseJSON.success = false;
                            } else {
                                responseJSON.data = this.cleanXMl(obj);
                            }
                        });
                    } else {
                        console.error("Body Empty");
                        responseJSON.success = false;
                    }
                } else {
                    responseJSON.message = response.message;
                    responseJSON.data = response.data;
                    responseJSON.success = false;
                    if(typeof response.data.data !== "undefined"){
                        xml2js.parseString(response.data.data, (err, result) => {
                            if (!err) {
                                let obj = this.cleanScheme(result);
                                console.log(obj);
                                if(typeof obj['soap:Fault'] !== "undefined") {
                                    responseJSON.data = this.cleanXMl(obj['soap:Fault']);
                                } else if (typeof obj['soapenv:Fault'] !== "undefined"){
                                    responseJSON.data = this.cleanXMl(obj['soapenv:Fault']);
                                } else {
                                    responseJSON.data = this.cleanXMl(obj);
                                }
                            }
                        });
                    }
                }

                resolve(responseJSON)

            } catch (error) {
                console.error(error);
                if (error.response) {
                    reject({
                        "success": false,
                        "message": "error 1",
                        "status": error.response.status,
                        "headers": error.response.headers,
                        "data": error.response.data
                    });
                } else if (error.request) {
                    reject({
                        "success": false,
                        "message": "error 2",
                        "data": error.request
                    });
                } else {
                    reject({
                        "success": false,
                        "message": "error 3",
                        "data": error.message
                    });
                }
            }

        });
    }

    cleanXMl(array) {
        let clean = Array.isArray(array)? []: {};
        if (Array.isArray(array)) {
            if (array.length === 1) {
                clean = this.cleanXMl(array[0])
            } else {
                array.forEach((element, index) => {
                    clean[index] = this.cleanXMl(element);
                });

                /*
                for (let [key, value] of array) {
                    if (key !== '$') {
                        //clean[key.replace("ns1:", "").replace("ns2:", "").replace("ser:", "")] = this.cleanXMl(value);
                    }
                }*/
            }
        } else if (typeof array === 'object') {
            if(typeof array['$'] !== "undefined"){
                delete array['$'];
            }
            if(Object.keys(array).length === 1 && typeof array['_'] !== "undefined"){
                clean = this.cleanXMl(array['_']);
            } else {
                Object.entries(array).forEach(([key, value]) => {
                    if (key !== '$') {
                        clean[key.replace("ns1:", "").replace("ns2:", "").replace("ns3:", "").replace("ser:", "")] = this.cleanXMl(value);
                    }
                });
            }
        } else {
            if((array == parseInt(array))){
                if(BigInt(array) != parseInt(array)){
                    clean = array.toString();
                } else {
                    if(array.toString().trim().length < 16){
                        if (array.charAt(0) == 0) {
                            clean = array;
                        } else {
                            clean = parseInt(array);
                        }
                    } else {
                        clean = array.toString();
                    }
                }
            } else {
                clean = array;
            }
        }
        return clean;
    }

}

module.exports = new Utils();
